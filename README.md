Zgulde's Python Utilities
=========================

A collection of miscellaneous helper functions and classes that I find useful.
Maybe you will too!

The most polished of all of these is the `extend_pandas` module. It provides
some extensions to [pandas](http://pandas.pydata.org/) that I've found useful.

To install/upgrade:

```
python -m pip install --upgrade zgulde
```

Usage:

```python
# from within a python script / jupyter notebook, etc
import zgulde.extend_pandas
# now you can use all the additional methods
```

[Further documentation can be found here](https://zach.lol/extend_pandas), or by
looking at the built-in help:

```python
import zgulde.extend_pandas
help(zgulde.extend_pandas)
```
